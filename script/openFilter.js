const sideBarBtn = document.querySelector('.button-nav');
const navList = document.querySelector('.nav__list');
const programsBtn = document.querySelector('.nav__button-progarms');

sideBarBtn.addEventListener('click', ()=>{
    sideBarToggle()
})
function sideBarToggle(){
    navList.classList.toggle('nav__list--active')
    sideBarBtn.classList.toggle('button-nav--active')
    programsBtn.classList.toggle('nav__button-progarms--active')
}