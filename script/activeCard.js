const plusBtnList = document.querySelectorAll('.button-plus')
const moreBtnList = document.querySelectorAll('.product__button')
const cardList = document.querySelectorAll('.details')
 
plusBtnList.forEach((plusBtn)=>{
    plusBtn.addEventListener("click",(event)=>{
        cardToggle(event.target.dataset.cardId)
        //event.target.classList.toggle('button-plus--active')
    })
})
moreBtnList.forEach((moreBtn)=>{
    moreBtn.addEventListener("click",(event)=>{
        cardToggle(event.target.dataset.cardId)
        //event.target.classList.toggle('product__button--active')
    })
})
function cardToggle(data){
    cardList.forEach((card)=>{
        if(card.dataset.cardId === data){
            card.classList.toggle('details--active')
        }
    })
    moreBtnList.forEach((moreBtn)=>{
        if(moreBtn.dataset.cardId === data){
            moreBtn.classList.toggle('product__button--active')
        }
    })
    plusBtnList.forEach((plusBtn)=>{
        if(plusBtn.dataset.cardId === data){
            plusBtn.classList.toggle('button-plus--active')
        }
    })
}
