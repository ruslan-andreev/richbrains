class Accordion {
    constructor(domNode) {
      this.rootEl = domNode;
      this.buttonEl = this.rootEl.querySelector('button[aria-expanded]');
      const controlsId = this.buttonEl.getAttribute('aria-controls');
      
      this.contentEl = document.getElementById(controlsId);
  
      this.open = this.buttonEl.getAttribute('aria-expanded') === 'true';
  
      
      this.buttonEl.addEventListener('click', this.onButtonClick.bind(this));

      this.linkList = [...this.rootEl.querySelectorAll('.nav__link')]
      this.linkList.forEach((element)=> {

        element.addEventListener('click', (event)=>this.onLinkClick(event))
      })
      
    } 
  
    onButtonClick() {
      this.toggle(!this.open);
    }

    onLinkClick(event) {
        this.toggleLink(event)
    }
  
    toggle(open) {
      
      if (open === this.open) {
        return;
      }
  
      this.open = open;
  
      
      this.buttonEl.setAttribute('aria-expanded', `${open}`);
      if (open) {
        this.buttonEl.classList.toggle('nav__button-link--active')
        this.contentEl.classList.toggle('link__list--active')
      } else {
        this.buttonEl.classList.toggle('nav__button-link--active')
        this.contentEl.classList.toggle('link__list--active')
      }
    }

    toggleLink(event) {
        event.preventDefault()
        this.linkList.forEach(link =>{
          if(link.hasAttribute('active')){
            link.removeAttribute('active')
          }
        })
        
        event.target.setAttribute('active', '')
    }
  
}

const accordions = document.querySelectorAll('.accordion');
accordions.forEach((accordionEl) => {
  new Accordion(accordionEl);
});